import Vue from 'vue'
import Router from 'vue-router'
import Nimix from '@/components/Nimix'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Nimix',
      component: Nimix
    }
  ]
})
