import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'
import router from './router'
import config from '@/config.js'

import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueResource)
Vue.use(BootstrapVue)

const store = new Vuex.Store({
  state: {
    gender: 2,
    nameSegments: {
      first: 50,
      middle: 50,
      last: 50
    },
    name: '',
    middleNameChecked: true,
    resultNames: undefined,
    resultRating: undefined,
    error: undefined
  },
  mutations: {
    setGender (state, gender) {
      state.gender = gender
    },
    setName (state, event) {
      state.name = event.target.value
    },
    setMiddleNameChecked (state, event) {
      state.middleNameChecked = event.target.checked
    },
    setSegments (state, payload) {
      state.nameSegments = Object.assign({}, state.nameSegments, payload)
    },
    updateResults (state, payload) {
      state.resultNames = payload.names
      state.resultRating = payload.rating
      state.error = payload.error
    }
  },
  actions: {
    getRandomNames ({commit, state}) {
      return new Promise((resolve, reject) => {
        const data = {
          op: 'randomnames',
          gender: state.gender,
          firstNameSegment: state.nameSegments.first,
          lastNameSegment: state.nameSegments.last,
          amount: 5
        }
        if (state.middleNameChecked) data.middleNameSegment = state.nameSegments.middle

        Vue.http.get(config.apiUrl, {params: data}).then(response => {
          commit('updateResults', { names: response.body.names })
          resolve()
        }, response => {
          commit('updateResults', { error: response.status + ': ' + response.bodyText })
          resolve()
        })
      })
    },
    getNameRating ({commit, state}) {
      return new Promise((resolve, reject) => {
        const data = {
          op: 'ratename',
          gender: state.gender,
          name: state.name
        }
        Vue.http.get(config.apiUrl, {params: data}).then(response => {
          commit('updateResults', { rating: response.body.rating })
          resolve()
        }, response => {
          commit('updateResults', { error: response.status + ': ' + response.bodyText })
          resolve()
        })
      })
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
