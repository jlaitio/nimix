const _ = require("lodash")

const data = require("./data.js")

function randomNames(gender, firstNameSegment, lastNameSegment, middleNameSegment, amount = 1) {
    const choice = (segment, data) => {
        const upperBound = Math.min(data.length, data.length*(segment + 2)/100)
        const lowerBound = Math.max(0, data.length*(segment - 2)/100)

        const index = Math.floor(lowerBound + Math.random()*(upperBound - lowerBound))
        return data[index][0]
    }
    return _.range(amount).map((i) => (choice(firstNameSegment, (gender == 1) ? data.menFirstNames : data.womenFirstNames)
        + " "
        + ((middleNameSegment)
           ? (choice(middleNameSegment, (gender == 1) ? data.menMiddleNames : data.womenMiddleNames)) + " "
           : "")
        + choice(lastNameSegment, data.lastNames)))
}

function rateName(gender, name) {
    const segment = (entry, data) => {
        const index = _.findIndex(data, (d) => (d[0] === entry))

        if (index === -1)
            throw new Error("Entry not found: " + entry)
        return index / data.length
    }

    const parts = name.split(/\s+/)
    if (parts.length < 2 || parts.length > 3)
        throw new Error("Erroneous name format")

    const firstNameRating = segment(parts[0], (gender == 1) ? data.menFirstNames : data.womenFirstNames)
    const lastNameRating = segment((parts.length === 2) ? parts[1] : parts[2], data.lastNames)

    if (parts.length === 2) {
        return (firstNameRating + lastNameRating) / 2
    } else {
        const middleNameRating = segment(parts[1], (gender == 1) ? data.menMiddleNames : data.womenMiddleNames)

        return (firstNameRating + middleNameRating + lastNameRating) / 3
    }
}

module.exports.handler = (event, context, callback) => {
    const responseObject = (status, bodyString) => ({
      statusCode: status,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: bodyString
    })

    const param = (name) => {
        if (!event.queryStringParameters || !event.queryStringParameters[name]) {
            callback(null, responseObject(400, `Missing param ${name}`))
            throw `Missing parameter ${name}`
        } else {
            return event.queryStringParameters[name]
        }
    }

    const paramOption = (name) => (event.queryStringParameters[name])


    try {
      switch (param("op")) {
          case "randomnames":
              const nameResponse = responseObject(200, JSON.stringify({
              names: randomNames(param("gender"), param("firstNameSegment"), param("lastNameSegment"), paramOption("middleNameSegment"), paramOption("amount"))
              }))
              callback(null, nameResponse);
              break
          case "ratename":
              const ratingResponse = responseObject(200, JSON.stringify({
              rating: rateName(param("gender"), param("name"))
              }))
              callback(null, ratingResponse);
              break
          default:
              callback(null, responseObject(400, `Unknown op ${param("op")}`));
      }
    } catch (e) {
      callback(null, responseObject(400, e.message))
    }
};
